package beergame

import (
	"fmt"

	"gitlab.com/APeculiarCamber/CS680-HW2/agent"
	"gitlab.com/akita/akita/v3/sim"
)

func makeFactory(engine sim.Engine) *agent.Agent {
	factory := agent.NewAgent("Factory", engine)
	factory.InventoryCost = 1
	factory.IsFactory = true
	return factory
}
func makeDistro(engine sim.Engine) *agent.Agent {
	distro := agent.NewAgent("Distro", engine)
	distro.InventoryCost = 2
	return distro
}
func makeWholesaler(engine sim.Engine) *agent.Agent {
	seller := agent.NewAgent("Wholesaler", engine)
	seller.InventoryCost = 3
	return seller
}
func makeRetailer(engine sim.Engine) *agent.Agent {
	retailer := agent.NewAgent("Retailer", engine)
	retailer.LostCustomerPenalty = 20
	retailer.InventoryCost = 4
	retailer.IsRetailer = true
	return retailer
}

// Assumes order -> Factory, Distro, Whole, Retailer
func setRelations(levels []*agent.Agent, engine sim.Engine) {
	numLevels := len(levels)
	for i := 0; i < numLevels-1; i++ {
		a, b := levels[i], levels[i+1]
		a.DownStreamAgent = b
		b.UpStreamAgent = a

		conn := sim.NewDirectConnection(a.Name()+"To"+b.Name()+"Conn", engine, 1)
		conn.PlugIn(a.DownStream, 1)
		conn.PlugIn(b.UpStream, 1)
	}
}

type NewCustomerEvent struct {
	time     sim.VTimeInSec
	handler  sim.Handler
	quantity int
}

func (e NewCustomerEvent) Time() sim.VTimeInSec {
	return e.time
}
func (e NewCustomerEvent) Handler() sim.Handler {
	return e.handler
}
func (e NewCustomerEvent) IsSecondary() bool {
	return false
}

type NewCustomerEventHandler struct {
	retailer *agent.Agent
}

func (h *NewCustomerEventHandler) Handle(evt sim.Event) error {
	//fmt.Println(fmt.Sprint(evt.Time()/2) + ": Handling Customer " + fmt.Sprintf("%d", evt.(NewCustomerEvent).quantity))
	order := agent.Order{Quatity: evt.(NewCustomerEvent).quantity}
	order.Src = nil
	order.Dst = h.retailer.DownStream
	h.retailer.DownStream.Recv(order)

	return nil
}

func scheduleCustomers(engine sim.Engine, handler *NewCustomerEventHandler) {
	for i := 0; i < 20; i++ {
		quantity := 16
		if i < 8 {
			quantity = 4
		}
		// Stagger the customers by 2 seconds so that we can guarentee that retailer can respond properly
		engine.Schedule(NewCustomerEvent{sim.VTimeInSec((i * 2) + 1), handler, quantity})
	}
}

func setHook(levels []*agent.Agent, hook *agent.EndDayHook) {
	for _, l := range levels {
		l.AcceptHook(hook)
	}
}

func RunBeerGame() {
	engine := sim.NewSerialEngine()
	factory := makeFactory(engine)
	distro := makeDistro(engine)
	wholesaler := makeWholesaler(engine)
	retailer := makeRetailer(engine)
	levels := []*agent.Agent{factory, distro, wholesaler, retailer}
	setRelations(levels, engine)

	handler := NewCustomerEventHandler{retailer: retailer}
	scheduleCustomers(engine, &handler)

	costCounter := agent.EndDayHook{}
	setHook(levels, &costCounter)

	engine.Run()

	fmt.Printf("The final cost was %d.\n", costCounter.Cost)
}
