package agent

import (
	"gitlab.com/akita/akita/v3/sim"
)

type Agent struct {
	*sim.TickingComponent

	UpStream        *sim.LimitNumMsgPort
	DownStream      *sim.LimitNumMsgPort
	UpStreamAgent   *Agent
	DownStreamAgent *Agent

	Inventory int
	Backlog   int
	OnOrder   int

	InventoryCost       int
	LostCustomerPenalty int
	IsRetailer          bool
	IsFactory           bool

	endTime sim.VTimeInSec
	Time    sim.VTimeInSec
}

func (a *Agent) Tick(now sim.VTimeInSec) bool {
	a.Time = now
	a.receiveOrder(now)
	a.receiveShipment(now)
	a.fulfillOrder(now)
	a.sendOrder(now)
	a.InvokeHook(sim.HookCtx{Domain: a, Pos: EndDayHookPos, Item: a})

	return a.endTime > now
}

func NewAgent(name string, engine sim.Engine) *Agent {
	agent := &Agent{}
	agent.TickingComponent = sim.NewTickingComponent(name, engine, sim.Freq(0.5), agent)
	agent.UpStream = sim.NewLimitNumMsgPort(agent, 2, name+"UpStream")
	agent.DownStream = sim.NewLimitNumMsgPort(agent, 2, name+"DownStream")
	agent.endTime = 40.0
	return agent
}

func (a *Agent) receiveOrder(now sim.VTimeInSec) {
	// - `receiveOrder`:
	// This stage should check the `Downstream` port for any incoming
	// messages (using the `Retrieve` method).
	// If there is a message, cast it to `Order` and add the quantity to the backlog.

	// There should be only 1, but we make sure to handle more?
	if msg := a.DownStream.Retrieve(now); msg != nil {
		a.Backlog += msg.(Order).Quatity
	}
}

/*
* `receiveShipment`: This stage should check the `Upstream` port for
* any incoming messages (using the `Retrieve` method).
* If there is a message, cast it to `Shipment` and add the quantity to the inventory.
 */
func (a *Agent) receiveShipment(now sim.VTimeInSec) {
	if msg, ok := a.UpStream.Retrieve(now).(Shipment); ok {
		//fmt.Printf("%s: %s got SHIPMENT from %s (I: %d+%d, O:%d-%d)\n",
		//	fmt.Sprint(now/2), a.Name(), msg.Src.Name(), a.Inventory, msg.Quatity, a.OnOrder, msg.Quatity)
		a.OnOrder -= msg.Quatity
		a.Inventory += msg.Quatity
	}
}

func (a *Agent) fulfillOrder(now sim.VTimeInSec) {
	// - `fulfillOrder`: This stage creates and sends a shipment to the downstream agent.
	//  The quantity to include in the shipment should equal the minimum of the inventory and the backlog.
	// If either the inventory or the backlog is 0, do not create a shipment. If the shipment is sent,
	// reduce the backlog and the inventory by the quantity of the shipment. After executing this stage,
	// either the backlog or the inventory should be 0. If the agent is retailer, simply update the variables but do not create a shipment.
	sendAmount := a.Backlog
	if sendAmount > a.Inventory {
		sendAmount = a.Inventory
	}

	a.Inventory -= sendAmount
	a.Backlog -= sendAmount

	if sendAmount > 0 && a.DownStreamAgent != nil {
		//fmt.Printf("%s: %s sent SHIPMENT to %s (B: %d-%d, I:%d-%d)\n",
		//	fmt.Sprint(now/2), a.Name(), a.DownStreamAgent.Name(), a.Backlog+sendAmount, sendAmount, a.Inventory+sendAmount, sendAmount)

		s := Shipment{Quatity: sendAmount}
		s.Src = a.DownStream
		s.Dst = a.DownStreamAgent.UpStream
		s.SendTime = now
		a.DownStream.Send(s)
	}
}

func determineOrderQuantity(name string, agent *Agent) int {
	if agent.DownStreamAgent == nil {
		return (2 * agent.Backlog) - agent.Inventory - agent.OnOrder
	} else {
		return agent.Backlog - agent.Inventory - agent.OnOrder
	}
}

func (a *Agent) sendOrder(now sim.VTimeInSec) {
	// - `sendOrder`: This stage creates and sends an order to the upstream agent.
	// You can decide the quantity in any way you want. However, a simple strategy is to
	// use quantity = backlog - inventory - onorder. This is very conservative; so you can
	// define a better algorithm. If the order is sent, update the onorder variable.
	// If the agent is the factory, simply update the inventory variable but do not create an order.

	quantity := determineOrderQuantity(a.Name(), a)
	if a.UpStreamAgent != nil {
		if quantity > 0 {
			o := Order{Quatity: quantity}
			o.Src = a.UpStream
			o.Dst = a.UpStreamAgent.DownStream
			o.SendTime = now
			a.UpStream.Send(o)

			a.OnOrder += quantity
		}
	} else {
		// fmt.Printf("Factory made %d with %d - %d - %d\n", quantity, a.Backlog, a.Inventory, a.OnOrder)
		a.Inventory += quantity
	}
}
