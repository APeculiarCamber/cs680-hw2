package agent

import (
	"gitlab.com/akita/akita/v3/sim"
)

// HookPosPortMsgSend marks when a message is sent out from the port.
var EndDayHookPos = &sim.HookPos{Name: "EndDayHook"}

type EndDayHook struct {
	Cost int
}

func (e *EndDayHook) Func(ctx sim.HookCtx) {
	if EndDayHookPos == ctx.Pos {
		if agent, ok := ctx.Domain.(*Agent); ok {
			//fmt.Printf("%d: %s with inventory %d and cost %d", int32(agent.Time/2), agent.Name(), agent.Inventory, agent.InventoryCost)
			e.Cost += agent.Inventory * agent.InventoryCost

			if agent.IsRetailer {
				//fmt.Printf(",  with customers %d and cost %d", agent.Backlog, agent.InventoryCost)
				e.Cost += agent.Backlog * agent.LostCustomerPenalty
				agent.Backlog = 0 // reset backlog, the customers leave.
			}
			//fmt.Println()
		}
	}
}
