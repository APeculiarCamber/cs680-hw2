//First, define a struct named `Order` that embeds
//`sim.MsgMeta` and a field called `Quatity` (type `int`).
//Also, add a method called `Meta` that returns the `sim.MsgMeta`
//field.
//You can see an example
//[here](https://gitlab.com/akita/akita/-/blob/86018d8332c098b6a632b44d1dea2976b2ee0551/sim/ping_test.go#L10).

// Similarly, define another struct named `Shipment`.
// The fields and methods should be the same as in `Order`.
package agent

import (
	"gitlab.com/akita/akita/v3/sim"
)

type Order struct {
	sim.MsgMeta

	Quatity int
}

func (o Order) Meta() *sim.MsgMeta {
	return &o.MsgMeta
}

type Shipment struct {
	sim.MsgMeta

	Quatity int
}

func (s Shipment) Meta() *sim.MsgMeta {
	return &s.MsgMeta
}
